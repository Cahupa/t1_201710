package model.logic;

import java.util.Iterator;

import model.data_structures.NumbersBag;

public class NumbersBagOperations<N> {



	public double computeMean(NumbersBag bag){
		Number mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<N> iter = bag.getIterator();
			while(iter.hasNext()){
				if(iter.next().equals(Long.)){
					
				}
				mean += iter.next();
				Class<? extends Number> cls = mean.getClass();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(NumbersBag bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}

		}
		return max;
	}

	public int sumar(NumbersBag bag){
		int total = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				total += iter.next();
			}
		}
		return total;
	}

	public int cantidad(NumbersBag bag){
		int count = 1;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				count++;
			}
		}
		return count;
	}

	public double promedio(NumbersBag bag){
		double promedio = 0;
		if(bag != null){
			promedio = sumar(bag)/cantidad(bag);
		}
		return promedio;
	}
}
