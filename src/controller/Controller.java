package controller;

import java.util.ArrayList;

import model.data_structures.NumbersBag;
import model.logic.NumbersBagOperations;

public class Controller {

	private static NumbersBagOperations model = new NumbersBagOperations();
	
	
	public static NumbersBag createBag(ArrayList<Number> values){
         return new NumbersBag(values);		
	}
	
	
	public static Number getMean(NumbersBag bag){
		return model.computeMean(bag);
	}
	
	public static Number getMax(NumbersBag bag){
		return model.getMax(bag);
	}
	
	public static Number getSum(NumbersBag bag)
	{
		return model.sumar(bag);
	}
	
	public static Number getQuantity(NumbersBag bag)
	{
		return model.cantidad(bag);
	}
	
	public static Number getPromedium(NumbersBag bag)
	{
		return model.promedio(bag);
	}
}
